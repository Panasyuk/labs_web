var data = { 'page': 0 }, permit_to_scroll = true, controller;

/* Upload new posts */
$(window).scroll(function() {
    if(($(window).scrollTop() + $(window).height() >= $(document).height() - 200) && permit_to_scroll) {
        data['page']++;
        get_posts();
    }
})

function clean_all() {
    data['page'] = 0;
    permit_to_scroll = true;
    $('.post_loading').show();
    $('.posts').empty();
}

function search_post() {
    val = $(".search-input input").val().replace(/\s+/g, " ");
    
    if(!data['search'] || data['search']!=val) {
        data['search'] = val;
        clean_all();
        get_posts();
    }

    if(val.replace(" ", "") == '') {
        delete data['search'];
    }
}

function sort_posts() {
    data['sort'] = $(".sorting").val();
    clean_all();
    get_posts();
}

function load_posts(ctrl) {
    controller = ctrl;
    get_posts();
}

function get_posts() {
    $.ajax({
            method: "GET",
            async: false,
            dataType: "HTML",
            url: "/" + controller + "/loadposts",
            data: data,
            success: function(data, textStatus, request) {
                $('.post_loading').hide();
                $('.posts').append(data);

                permit_to_scroll = (request.getResponseHeader('permit_to_scroll') == 'true');

                check_empty_posts();
            }
    });
}

function check_empty_posts() {
    if(!$(".post").length) {
        $(".posts").text("No posts");
    }
}

function set_category(category_id) {
    data['category_id'] = category_id;
}
