$(document).ready(function(){
    /* Videos */
    var prev;
    $(".image-thumbnail").click(function(e) {
        e.preventDefault();
        th = $(this);
        prev = th.find(".loading-preview")
        prev.append($("#floatingCirclesG").clone());
        prev.show();

        player = new YT.Player(th.find("img").attr("id"), {
          height: '180',
          width: '320',
          playerVars: {
                    autoplay: 1,
                    controls: 0,
                    showinfo: 0,
                    rel: 0,
                    modestbranding: 1,
                    vq: 'hd720'},
          videoId: th.data("youtube-id"),
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
    })

    function onPlayerReady(event) {
        event.target.playVideo();
        event.target.mute();
        setTimeout(function() { prev.hide() }, 1000);
    }

    function onPlayerStateChange(event) {
    }

    function stopVideo(event) {
        event.target.stopVideo();
    }

})

function getCSRFToken(xhr) {
    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
}

function change_right_menu_width(ih, ch) {
    menu_links = $(".right-menu-links");
    ih = 48;
    ch = menu_links.height();

    if(ch > ih) {
        menu_links.find("a:last").hide();
        visible_elements = menu_links.find("a:visible");
        visible_elements.css("width", menu_links.width() / visible_elements.length);
    }
    else {
        
    }
}

function check_mobile_menu() {
    console.log($(window).width());
    if($(window).width() < 978) {
        $(".menu").addClass("mobile-menu");
        $(".mobile-menu").removeClass("menu");
    }
    else {
        $(".mobile-menu").addClass("menu");
        $(".menu").removeClass("mobile-menu");
    }
}

function rotateAboutBlock() {
    var initial_width = 1520, 
        initial_border_width = 70,
        initial_about_center_height = 380,
        initial_border_top_width = 400,
        initial_about_height = 500,
        initial_margin_top_about_content = 40,
        current_window_width = $(window).width(),
        borderWidth = $("#about").width() / 2;

    var koef = current_window_width / initial_width;

    // Border top width: init width divided by window width, then increase the init border width
    btp = initial_width / current_window_width * initial_border_width;
    bwidth = btp + "px 133px 0px";
    // About height
    ah = koef * initial_about_height;
    // Set margin top of about ocntent
    mtac = koef * initial_margin_top_about_content;
    // About center height
    if($("#triangle").is(":visible")){
        ach = koef * initial_about_center_height;
        abh = ach + 100; // padding bottom 100
    }
    else {
        ach = $(".about-content").height() + 40 + mtac; // 40 - margin-bottom; mtac - new margin top
        abh = ach + 20;
    }
    // Border-top width
    btw = koef * initial_border_top_width;

    $("#triangle").css({
        "border": borderWidth + "px solid transparent",
        "border-top": btw + "px solid #09010e"
    });
    // $(".about-shadow").css("border-width", bwidth);
    // $(".about-center").css("height", ach);
    // $("#about").css("height", abh);
}

function set_dimension(ths) {
    img = $(ths);
    width = img.width, height = img.height;
    if(width > height)
        klass = "wbth";
    else
        klass = "hbtw"
    img.addClass(klass);
}

function change_popup_register(th) {
    ths = $(th);
    active_klass = "active-popup";
    if(!ths.hasClass(active_klass)) {
        $("." + active_klass).removeClass(active_klass);
        $(".login, .register").hide();
        ths.addClass(active_klass);
        $(ths.data("class")).show();
    }
    return false;
}

function open_popup(menuclass) {
    if(menuclass)
        change_popup_register($("body a[data-class='" + menuclass + "']"));
    $(".wrap-popup").show();
    return false;
}

$(".login form").submit(function(e) { 
    e.preventDefault();
    th = $(this);
    $.ajax({
            method: "POST",
            async: false,
            dataType: "JSON",
            url: "/login",
            beforeSend: function(xhr) { getCSRFToken(xhr) },
            data: th.serialize(),
            successs: function(data) {
                console.log(data);
            }
    });
    return false;
});

// Click out of the POPUP
$(document).mouseup(function(e) {
    var container = $(".popup");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $(".wrap-popup").hide();
    }
});


/* --------------------- MOBILE SCRIPT ------------------------------- */
var startX, endX;

function toggle_menu(width) {
    $(".mobile-menu").animate({ "width": width}, 200);
    setTimeout(function() {
        $(".small-menu").css("position", width == 0 ? "absolute" : "fixed")
    }, 100);
}

function open_menu() {
    width = $(".mobile-menu").width() > 0 ? 0 : "40%";
    toggle_menu(width);
    return false;
}

$(document)
  .mouseup(function(e){
    if($(window).width() <= 992) {
        endX = e.pageX;
        if(startX - endX > 100)
            toggle_menu(0);
        else if(startX - endX < -200) 
            toggle_menu("40%");
    }
  })
  .mousedown(function(e) {
    if($(window).width() <= 992) {
        startX = e.pageX;
    }
  });

//   $(document).mousemove(function(e){
//     if($(window).width() <= 992) {
//         endX = e.pageX;
//         if(startX) {
//             if(startX - endX > 100)
//                 toggle_menu(0);
//             else if(startX - endX < -100) 
//                 toggle_menu("40%");
//         }
//         startX = endX;
//     }
// })
